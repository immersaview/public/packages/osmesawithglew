cmake_minimum_required(VERSION 3.0)

set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}")

if ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "aarch64")
    set(ARCH_DIR "arm64")
elseif ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "AMD64|x86_64")
    if("${CMAKE_SIZEOF_VOID_P}" STREQUAL "4")
        set(ARCH_DIR "x86")
    elseif ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "8")
        set(ARCH_DIR "x64")
    endif ()
else ()
    message(FATAL_ERROR "Unable to determine CPU architecture")
endif ()

if (WIN32)
    add_definitions(-DGLEW_STATIC)
    set(GLEW_LIB_NAME_RELEASE "glew32s.lib")
    set(GLEW_LIB_NAME_DEBUG "glew32sd.lib")
endif ()

function(get_glew_libs USE_OSMESA)
if (${USE_OSMESA})
	set(GLEW_LIBS glew_osmesa osmesa)
else ()
	set(GLEW_LIBS glew)
endif ()
endfunction(get_glew_libs)

function(add_imported_lib LIB LIB_NAME_RELEASE LIB_NAME_DEBUG)
    message(STATUS "Adding library '${LIB}'. Reference the library using the TARGET '${LIB}'")
    add_library("${LIB}" STATIC IMPORTED GLOBAL)
    set_property(TARGET "${LIB}" APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_LIST_DIR}/include")
    set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_RELEASE "${CMAKE_CURRENT_LIST_DIR}/lib/win/vc142/${ARCH_DIR}/release/${LIB_NAME_RELEASE}")
    set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_DEBUG "${CMAKE_CURRENT_LIST_DIR}/lib/win/vc142/${ARCH_DIR}/debug/${LIB_NAME_DEBUG}")
endfunction(add_imported_lib)

function(add_imported_dynlib LIB DLL_NAME LIB_NAME)
    message(STATUS "Adding library '${LIB}'. Reference the library using the TARGET '${LIB}'")
    add_library("${LIB}" SHARED IMPORTED GLOBAL)
    set_property(TARGET "${LIB}" APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_LIST_DIR}/include")
    set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_RELEASE "${CMAKE_CURRENT_LIST_DIR}/bin/win/vc142/${ARCH_DIR}/release/${DLL_NAME}")
    set_target_properties("${LIB}" PROPERTIES IMPORTED_IMPLIB_RELEASE "${CMAKE_CURRENT_LIST_DIR}/lib/win/vc142/${ARCH_DIR}/release/${LIB_NAME}")
    set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_DEBUG "${CMAKE_CURRENT_LIST_DIR}/bin/win/vc142/${ARCH_DIR}/debug/${DLL_NAME}")
    set_target_properties("${LIB}" PROPERTIES IMPORTED_IMPLIB_DEBUG "${CMAKE_CURRENT_LIST_DIR}/lib/win/vc142/${ARCH_DIR}/debug/${LIB_NAME}")
endfunction(add_imported_dynlib)

add_imported_lib("glew_osmesa" "${GLEW_LIB_NAME_RELEASE}" "${GLEW_LIB_NAME_DEBUG}")
add_imported_dynlib("osmesa" "osmesa.dll" "osmesa.lib")

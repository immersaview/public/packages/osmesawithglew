[binaries]
c = 'cl'
cpp = 'cl'
ar = 'lib'
strip = 'strip'
windres = 'rc'

[host_machine]
system = 'windows'
cpu_family = 'x86_64'
cpu = 'i686'
endian = 'little'

[properties]
llvmlibdir = 'lib64'